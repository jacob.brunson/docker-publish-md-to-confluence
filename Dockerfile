FROM python:3.12

WORKDIR /usr/src/app

RUN apt update && apt install -y ca-certificates-java && apt install -y plantuml graphviz npm chromium && apt clean
RUN npm install -g @mermaid-js/mermaid-cli
ADD https://github.com/plantuml/plantuml/releases/download/v1.2023.9/plantuml-1.2023.9.jar /usr/share/plantuml/plantuml.jar
COPY requirements.txt ./
COPY puppeteer-config.json ./
RUN pip install --no-cache-dir -r requirements.txt

COPY *.py .

CMD ["python3", "entrypoint.py"]
