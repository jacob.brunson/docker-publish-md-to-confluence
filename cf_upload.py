import sys
import re
import os
import plantuml
import yaml
import tempfile
import subprocess
from PIL import Image
from io import BytesIO
from pathlib import Path
from jacobsjsonschema.draft7 import Validator
import json
import jacobsjsondoc
from dataclasses import dataclass

@dataclass
class ConfluenceConnection:
    url: str
    username: str
    password: str
    space: str|None
    dry_run: bool=False

    def to_cmd_args(self) -> list[str]:
        args = ["--host", self.url, "--username", self.username, "--password", self.password]
        return args



def replace_diagrams(md_filename):

    pu = plantuml.PlantUML(url="http://www.plantuml.com/plantuml/img/")

    with open(md_filename) as md_file:
        md_contents = md_file.read()

    pattern = r"^(```plantuml\n(.*?)\n```\n)"

    def replacement_image(match_obj: re.Match) -> str:
        uml = match_obj.group(2)
        url = pu.get_url(uml)
        image_data = pu.processes(uml)
        image_bytes = BytesIO(image_data)
        img = Image.open(image_bytes)
        width, height = img.size
        return f'<img src="{url}" width="{width}" height="{height}" alt="PlantUML diagram"></img>\n'

    md_contents = re.sub(pattern, replacement_image, md_contents, flags=re.DOTALL|re.MULTILINE)
    return md_contents

def upload_file(md_filename: Path, 
    conf_con: ConfluenceConnection,
    page_id: str|int|None=None, 
    title: str|None=None, 
    append: list[Path]|None=None, 
    parent: str|int|None=None,
    commit_message: str|None=None
):
    cmd = ['md2cf', '--only-changed', '--strip-top-header', '--enable-relative-links']

    with tempfile.NamedTemporaryFile(mode="w", delete=True, delete_on_close=False) as upload_fp:

        upload_fp.write(replace_diagrams(md_filename))

        if isinstance(append, list):
            for additional_md in append:
                upload_fp.write(replace_diagrams(additional_md))

        upload_fp.close()

        if page_id is not None:
            cmd.extend(['--page-id', str(page_id)])

        if title is not None:
            cmd.extend(['--title', title])
        
        if conf_con.space is not None:
            cmd.extend(['--space', conf_con.space])
        else:
            raise Exception("Confluence space is not defined")

        if isinstance(parent, int):
            cmd.extend(['--parent-id', str(parent)])
        elif isinstance(parent, str):
            cmd.extend(['--parent-tile', parent])

        #if commit_message := os.environ.get('CI_COMMIT_MESSAGE', False):
        if commit_message is not None:
            cmd.extend(["--message", str(commit_message)])

        cmd.extend(conf_con.to_cmd_args())

        cmd.append(upload_fp.name)

        print(" ".join(cmd))
        if conf_con.dry_run is False:
            subprocess.run(cmd)

def main(yaml_path: Path, conf_con: ConfluenceConnection):
    overall_space = conf_con.space
    with tempfile.TemporaryDirectory() as tempdir:
        with open(yaml_path, "r") as md_fp:
            instructions = jacobsjsondoc.parse(md_fp.read())

            with open("schema.json", "r") as schema_fp:
                schema = jacobsjsondoc.parse(schema_fp.read())

            validator = Validator(schema)
            validator.validate(instructions)

            project_dir = os.environ.get('PROJECT_DIR', yaml_path.parent)

            for i, doc in enumerate(instructions['docs']):
                assert 'filename' in doc, f"No 'filename' indicated for doc #{i}"
                md_filename = project_dir / doc['filename']
                conf_con.space = doc.get('space', overall_space)
                kwargs = dict()
                if page_id := doc.get('page_id'):
                    kwargs['page_id'] = page_id
                if title := doc.get('title'):
                    kwargs['title'] = title
                if isinstance(doc.get('concat'), list):
                    kwargs['append'] = doc.get('concat', [])
                if isinstance(doc.get('parent_id'), (str, int)):
                    kwargs['parent'] = int(doc.get('parent'))
                elif isinstance(doc.get('parent_title'), str):
                    kwargs['parent'] = doc.get('parent_title')
                if commit_message := os.environ.get('CI_COMMIT_MESSAGE', False):
                    kwargs['commit_message'] = commit_message

                upload_file(md_filename, conf_con, **kwargs)
