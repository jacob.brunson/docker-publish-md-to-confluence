import typer
from typing_extensions import Annotated
from cf_upload import main as do_upload, ConfluenceConnection, upload_file
from pathlib import Path
from typing import Optional

app = typer.Typer()

@app.command()
def upload(
    yaml_file_list: Path,
    confluence_host: Annotated[str, typer.Option(envvar="CONFLUENCE_HOST")],
    username: Annotated[str, typer.Option(envvar="CONFLUENCE_USERNAME")],
    token: Annotated[str, typer.Option(envvar="CONFLUENCE_PASSWORD")],
    space: Annotated[Optional[str], typer.Option(envvar="CONFLUENCE_SPACE")],
):
    cc = ConfluenceConnection(url=confluence_host, username=username, password=token, space=space)
    do_upload(yaml_file_list, cc)


@app.command()
def uploadone(
    filename: Path,
    confluence_host: Annotated[str, typer.Option(envvar="CONFLUENCE_HOST")],
    username: Annotated[str, typer.Option(envvar="CONFLUENCE_USERNAME")],
    token: Annotated[str, typer.Option(envvar="CONFLUENCE_PASSWORD")],
    space: Annotated[Optional[str], typer.Option(envvar="CONFLUENCE_SPACE")],
):
    cc = ConfluenceConnection(url=confluence_host, username=username, password=token, space=space)
    upload_file(filename, cc)


if __name__ == "__main__":
    app()