Docker image to publish markdown to confluence

Call with a YAML file that looks like this:

```yaml
docs:
    - filename: foo.md
    - filename: temp.md
      concat:
        - a.md
        - b.md
      title: A collection of things
    - filename: something.md
      space: OTHER
```

Example usage:
```sh
python3 entrypoint.py \
  --confluence-host=https://example.atlassian.net/wiki/rest/api \
  --token=AEXAMPLE3 \
  --space='~example.user' \
  --username 'example.user@example.com' \
  example/instructions.yaml
```
