
from atlassian import Confluence
import os.path
import sys
import yaml
import requests
import subprocess
import re
import marko
import tempfile

class Uploader():

    def __init__(self, confluence_username, confluence_password):
        self.host = os.environ.get('CONFLUENCE_HOST', False)
        self._username = confluence_username
        self._password = confluence_password
        assert self.host
        self.confl = Confluence(
            url=self.host,
            username=confluence_username,
            password=confluence_password
        )

    def has_changed(self, uml: str, parent_id, attachment_name):
        results = self.confl.get_attachments_from_content(parent_id, filename=attachment_name, media_type='text/plain')
        if len(results["results"]) != 1:
            return True
        url = f"{self.host}{results['results'][0]['_links']['download']}"
        resp = requests.get(url, auth=(self._username, self._password))
        if resp.status_code == 200:
            if uml == resp.content:
                return False
        return True

    def generate_image(self, uml):
        process = subprocess.Popen(
            ["plantuml", "-pipe"],
            stdin=subprocess.PIPE,
            stdout=subprocess.PIPE,
        )
        stdout, _ = process.communicate(uml)
        return stdout

    def generate_mermaid_image(self, mm):
        with tempfile.NamedTemporaryFile(suffix='.png') as temp_file:
            process = subprocess.Popen(
                ["mmdc", "--puppeteerConfigFile", "/usr/src/app/puppeteer-config.json", "--input", "-", "--outputFormat", "png", "--output", temp_file.name],
                stdin=subprocess.PIPE,
                stdout=subprocess.PIPE,
                stderr=subprocess.PIPE,
            )
            _out, err = process.communicate(mm)
            if process.returncode != 0:
                print(f"Mermaid error:\n{err}")
            return temp_file.read()

    def upload_plantuml(self, uml:bytes, diagram_name: str, confluence_space:str, parent_id:str|int):
        if self.has_changed(uml, parent_id, diagram_name):
            png_raw_data = self.generate_image(uml)
            print(f"Uploading {diagram_name}")
            self.confl.attach_content(
                content=uml,
                name=diagram_name,
                content_type='text/plain',
                page_id=str(parent_id),
                space=confluence_space,
                comment=os.environ.get('CI_COMMIT_MESSAGE', None),
            )
            self.confl.attach_content(
                content=png_raw_data,
                name=f"{diagram_name}.png",
                content_type='image/png',
                page_id=str(parent_id),
                space=confluence_space,
                comment=os.environ.get('CI_COMMIT_MESSAGE', None),
            )
        else:
            print(f"Skipping... {diagram_name} has not changed")

    def upload_mermaid(self, mm:bytes, diagram_name: str, confluence_space:str, parent_id:str|int):
        if self.has_changed(mm, parent_id, diagram_name):
            png_raw_data = self.generate_mermaid_image(mm)
            print(f"Uploading {diagram_name}")
            self.confl.attach_content(
                content=mm,
                name=diagram_name,
                content_type='text/plain',
                page_id=str(parent_id),
                space=confluence_space,
                comment=os.environ.get('CI_COMMIT_MESSAGE', None),
            )
            self.confl.attach_content(
                content=png_raw_data,
                name=f"{diagram_name}.png",
                content_type='image/png',
                page_id=str(parent_id),
                space=confluence_space,
                comment=os.environ.get('CI_COMMIT_MESSAGE', None),
            )
        else:
            print(f"Skipping... {diagram_name} has not changed")

    def upload_plantuml_file(self, path_to_diagram:str, confluence_space:str, parent_id:str|int):
        with open(path_to_diagram, 'rb') as fp:
            uml = fp.read()
        self.upload_plantuml(uml, os.path.basename(path_to_diagram), confluence_space, parent_id)

    def upload_mermaid_file(self, path_to_diagram:str, confluence_space:str, parent_id:str|int):
        with open(path_to_diagram, 'rb') as fp:
            uml = fp.read()
        self.upload_mermaid(uml, os.path.basename(path_to_diagram), confluence_space, parent_id)

    def upload_diagram_from_md(self, path_to_markdown, lang:str, confluence_space, parent_id, ident:str|int=0):
        ext = {'mermaid':'mmd', 'plantuml':'plantuml'}[lang]
        if isinstance(ident, str):
            upload_filename = f"{ident}.{lang}"
        elif isinstance(ident, int) and ident != 0:
            upload_filename = f"{os.path.basename(path_to_markdown).rstrip('.md')}_{ident}.{ext}"
        else:
            upload_filename = os.path.basename(path_to_markdown).replace('.md', f'.{ext}')

        with open(path_to_markdown, 'r') as fp:
            md = fp.read()

        doc = marko.parse(md)
        countdown = ident if isinstance(ident, int) else 100
        for node in doc.children:
            if node.get_type() == 'FencedCode' and node.lang == lang:
                image_id = node.extra.strip('#')
                if image_id == ident or countdown <= 0:
                    uml = node.children[0].children
                    print(f"Extracting diagram {ident if ident != 0 else ''} from {path_to_markdown} and uploading to {upload_filename}")
                    if lang == 'plantuml':
                        return self.upload_plantuml(uml.encode(), upload_filename, confluence_space, parent_id)
                    elif lang == 'mermaid':
                        return self.upload_mermaid(uml.encode(), upload_filename, confluence_space, parent_id)
                    else:
                        print("Unknown diagram type {node.lang}")
                else:
                    countdown -= 1
        print(f"Could not find {ident if ident != 0 else ''} diagram from {path_to_markdown}")

def process_yaml(uploader:Uploader):
    with open(sys.argv[1]) as md_fp:
        instructions = yaml.load(md_fp, Loader=yaml.SafeLoader)
        global_space = None
        if 'CONFLUENCE_SPACE' in os.environ:
            global_space = os.environ['CONFLUENCE_SPACE']
        elif 'space' in instructions:
            global_space = instructions['space']

        for diag in instructions.get('plantuml', list()):
            assert "filename" in diag
            assert "page_id" in diag
            space = diag.get('space', global_space)
            assert space is not None
            diag_filename = os.path.join(os.environ.get('PROJECT_DIR', ''), diag['filename'])
            if diag_filename.endswith(".plantuml"):
                uploader.upload_plantuml_file(diag_filename, space, parent_id=diag['page_id'])
            elif diag_filename.endswith(".md"):
                image_id = diag.get('id', 0)
                uploader.upload_diagram_from_md(path_to_markdown=diag_filename, lang='plantuml', confluence_space=space, parent_id=diag['page_id'], ident=image_id)
            else:
                print(f"Unknown file extension {diag_filename}. Doesn't look like plantuml")
                
        for diag in instructions.get('mermaid', list()):
            assert "filename" in diag
            assert "page_id" in diag
            space = diag.get('space', global_space)
            assert space is not None
            diag_filename = os.path.join(os.environ.get('PROJECT_DIR', ''), diag['filename'])
            if diag_filename.endswith(".mmd"):
                uploader.upload_mermaid_file(diag_filename, space, parent_id=diag['page_id'])
            elif diag_filename.endswith(".md"):
                image_id = diag.get('id', 0)
                uploader.upload_diagram_from_md(path_to_markdown=diag_filename, lang='mermaid', confluence_space=space, parent_id=diag['page_id'], ident=image_id)
            else:
                print(f"Unknown file extension {diag_filename} (doesn't look like mermaid)")
                    
                

if __name__ == '__main__':
    user = os.environ.get('CONFLUENCE_USERNAME', False)
    assert user
    passw = os.environ.get('CONFLUENCE_PASSWORD', False)
    assert passw
    space = os.environ.get('CONFLUENCE_SPACE', False)
    
    upldr = Uploader(user, passw)

    process_yaml(upldr)


